  SELECT
    calendar_week_number,
    time_id,
    day_name,
    sales,
    SUM(sales) OVER (ORDER BY calendar_week_number, time_id) AS cum_sum,
    AVG(sales) OVER (
      PARTITION BY calendar_week_number
      ORDER BY time_id
      ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING
    ) AS centered_3_day_avg
  FROM
    sales_data

SELECT
  calendar_week_number,
  time_id,
  day_name,
  sales,
  cum_sum,
  centered_3_day_avg
FROM
  WeeklySales
WHERE
  calendar_week_number IN (49, 50, 51)
ORDER BY
  calendar_week_number, time_id;
